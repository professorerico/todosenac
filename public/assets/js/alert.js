function apagarTarefa(id) {
    event.preventDefault()
    Swal.fire({
        title: 'Confirmação',
        text: 'Confirma a exclusão do Registro?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Apagar',
        cancelButtonText: 'Não apagar'
    }).then((result) => {
        if (result.isConfirmed) {
            window.location.href = 'remover/' + id
            Swal.fire({
                text: 'Tarefa Apagada',
                icon: 'success',
                showConfirmButton: false,
                timer: 1500
            })
        }
    })
}

const elementClose = document.querySelector('#close')
const buttonclose = document.querySelector('.close')

function fechaalerta() {
    elementClose.style.display = 'none'
}

const elemento = document.querySelector('.imprimir')

elemento.addEventListener('click', function() {
    var conteudo = document.getElementById('print').innerHTML
    var tela = window.open('about:blank')

    tela.document.write(conteudo)
    tela.window.print()
    tela.window.close()
})