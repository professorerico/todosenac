<!-- inicio do footer -->
</div>
</main>
<!-- /.container -->
<!-- Jquery BootStrap -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
    integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
</script>

<script src="/assets/js/bootstrap.bundle.min.js"></script>

<!-- SweetAlert2 -->
<script src="/assets/sweetalert2/sweetalert2.all.min.js"></script>
<script src="/assets/js/alert.js"></script>

</body>

</html>