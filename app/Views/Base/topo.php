<!DOCTYPE html>
<html lang="pt-br">
<?=helper('util'); ?>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors" />
    <meta name="generator" content="Hugo 0.80.0" />
    <title>Projeto ToDO | PSG 2021</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.6/examples/starter-template/" />

    <!-- Bootstrap core CSS -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Font Aewsome -->
    <link rel="stylesheet" href="/assets/css/fontawesome.css" />

    <!-- Favicons -->
    <meta name="theme-color" content="#563d7c" />

    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="/assets/sweetalert2/sweetalert2.min.css" />

    <style>
    .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    @media (min-width: 768px) {
        .bd-placeholder-img-lg {
            font-size: 3.5rem;
        }
    }
    </style>
</head>

<body>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark">
        <a class="navbar-brand " href="#">ToDO | PSG 2021</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
            aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                <li class='nav-item active'>
                    <a class="nav-link" href="<?=base_url()?>">Início</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown"
                        aria-expanded="false">Tarefas</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown01">
                        <a class="dropdown-item" href="<?=base_url('tarefa')?>">Tarefas</a>
                        <a class="dropdown-item" href="<?=base_url('prioridade')?>">Prioridades</a>
                        <a class="dropdown-item" href="<?=base_url('status') ?>">Status</a>
                        <a class="dropdown-item" href="<?=base_url('categoria')?>">Categorias</a>
                        <div class="dropdown-divider" href="#"></div>
                        <a class="dropdown-item" href="<?=base_url('concluidas')?>"> Tarefas Concluídas</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown02" data-toggle="dropdown"
                        aria-expanded="false">Usuario</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown02">
                        <a class="dropdown-item" href="<?=base_url('usuario')?>">Cadastro</a>
                        <a class="dropdown-item" href="<?=base_url('usuarios')?>">Prioridades</a>
                        <a class="dropdown-item" href="<?=base_url('status') ?>">Status</a>
                        <a class="dropdown-item" href="<?=base_url('categoria')?>">Categorias</a>
                        <div class="dropdown-divider" href="#"></div>
                        <a class="dropdown-item" href="<?=base_url('concluidas')?>"> Tarefas Concluídas</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled text-light" href="#" tabindex="-1"
                        aria-disabled="true"><?=exibe_data(date('Y-m-d'))?></a>
                </li>
            </ul>
            <?php if(session()->isLoggedIn) : ?>
            <form class="form-inline mt-2 mt-md-0" method="post">
                <input class="form-control mr-sm-2" type="text" value="Usuário: <?= strtoupper(session()->usuario)?>"
                    disabled aria-label="Usuario">
                <a href="<?=base_url('login/sair')?>" class="btn btn-outline-danger my-2 my-sm-0" type="submit"><i
                        class="fas fa-sign-out-alt"></i> Sair</a>
            </form>
            <?php endif ?>
        </div>
    </nav>

    <main role="main" class="container">
        <div class="">
            <h1>ToDO | PSG Senac 2021</h1>
            <!-- fim do header -->