<div class="container-sm mt-3">
    <form method="post">
        <fieldset class="form-group">
            <legend class="col-form-label">Novo Status:</legend>
            <div class="form-group">
                <label class="col-form-label" for="status">Status</label>
                <input class="form-control" class="form-control is-invalid" type="text" name="status" id="status"
                    placeholder="Digite o nome do status"
                    <?=(isset($novoStatus) ? "value='{$novoStatus['sta_nome']}'" : "" )?>required />
                <?php if($msg != '')
                    foreach ($msg as $key => $error) {
                        if($key == 'sta_nome')
                        echo (
                            "<div class=\"alert-sm alert-danger \">
                            {$error}
                            </div>"
                        );
                    };?>
                <label class="col-form-label" for="descricao">Descrição</label>
                <textarea class="form-control" class="form-control is-invalid" type="text" name="descricao"
                    id="descricao" placeholder="Digite uma descrição para o  status" rows=3>
                    <?=((isset($novoStatus) && strlen($novoStatus['sta_descricao']) > 0) ? "{$novoStatus['sta_descricao']}" : "" )?>
                </textarea>
                <?php if($msg != '')
                    foreach ($msg as $key => $error) {
                        if($key == 'sta_descricao')
                        echo (
                            "<div class=\"alert-sm alert-danger\">
                            {$error}</div>"
                        );
                    };?>
            </div>
            <div class="container-sm mt-3">
                <button class="btn btn-primary" type="submit"><i class="fas fa-plus"></i> <?=$acao?></button>
                <a href="<?=base_url()?>" class="btn btn-danger"><i class="far fa-arrow-alt-circle-left"></i> Voltar</a>
            </div>
        </fieldset>
    </form>
    <script>
    // Swal.fire($msg)
    </script>
</div>
<div class="container-sm p-md-5">
    <?php if (!is_null($status)) : ?>
    <table class="table table-hover table-striped text-center">
        <thead>
            <th>Código</th>
            <th>Status</th>
            <th>Descrição</th>
        </thead>
        <tbody>
            <?php foreach ($status as $sta) : ?>
            <tr>
                <td><?php echo($sta['sta_id']); ?></td>
                <td><?php echo($sta['sta_nome']); ?></td>
                <td><?php echo($sta['sta_descricao']); ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php endif; ?>
</div>