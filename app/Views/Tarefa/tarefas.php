    <div class="container mt-3">
        <div class="alert-success">
            <h5><?=$msg?></h5>
        </div>
        <div class="container-sm p-md-5 w-100">
            <?php if (!is_null($tarefas)) : ?>
            <table class="table table-hover table-striped text-center" id="print">
                <thead>
                    <th>Tarefa</th>
                    <th>Descrição</th>
                    <th>Prioridade</th>
                    <th>Status</th>
                    <th>Categoria</th>
                    <th>Data Entrega</th>
                    <?php if($opcao === "Ações"):?>
                    <th><?=$opcao;?></th>
                    <?php endif ?>
                </thead>
                <tbody>
                    <tr>
                        <?php foreach ($tarefas as $tarefa) : ?>
                        <td>
                            <?php echo($tarefa['tar_nome']); ?></td>
                        <td>
                            <?php echo($tarefa['tar_descricao']); ?></td>
                        <td>
                            <?php echo(traduzPrioridade($tarefa['tar_pri_id'])); ?></td>
                        <td>
                            <?php echo(traduzStatus($tarefa['tar_sta_id'])); ?></td>
                        <td>
                            <?php echo(traduzCategoria($tarefa['tar_cat_id'])); ?></td>
                        <td>
                            <?php echo(formatadataBd((string)$tarefa['tar_dataentrega'])); ?></td>
                        <td>
                            <?php if($opcao === 'Ações') :?>
                            <div class="container-sm w-auto ">
                                <a class="btn btn-sm btn-info" href="<?=base_url('atualizar/'.$tarefa['tar_id'])?>"><i
                                        class="far fa-edit"></i></a>
                                <a class="btn btn-sm btn-danger" onclick="apagarTarefa(<?=$tarefa['tar_id']?>)"
                                    href="<?=base_url('remover/'.$tarefa['tar_id'])?>"" id=" apagarTarefa"><i
                                        class="far fa-minus-square"></i></a>
                            </div>
                            <?php endif ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <button class="btn btn-success imprimir">Imprimir</button>
            <?php endif; ?>
        </div>
    </div>