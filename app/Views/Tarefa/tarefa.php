<div class="container-sm mt-3">
    <form method="post">
        <input type="hidden" name="id" value="<?=$acao != 'Inserir' ? $tarefa['tar_id'] : "" ?>">
        <fieldset class="form-group">
            <legend class="col-form-label"><?php echo $acao." Tarefa"?>
            </legend>
            <div class="d-flex justify-content-between align-items-center">
                <div class="form-group w-25">
                    <label class="col-form-label" for="tarefa">Tarefa</label>
                    <input class="form-control" class="form-control is-invalid" type="text" name="tarefa" id="tarefa"
                        placeholder="Digite o nome da tarefa"
                        <?php echo $acao === "Incluir" ? "" : "value=\"{$tarefa['tar_nome']}\""?> required />
                </div>
                <div class="form-group w-50">
                    <label class="col-form-label for=" descricao">Descrição</label>
                    <textarea class="form-control" type="text" name="descricao" id="descricao"
                        placeholder="Opcional: Informe a descrição da tarefa"><?php echo $acao === "Incluir" ? "" : "{$tarefa['tar_descricao']}"?></textarea>
                </div>
            </div>
            <div class="d-flex justify-content-between">
                <div class="form-group">
                    <label for="prioridade">Prioridade:</label>
                    <select id="prioridade" class="custom-select" name="prioridade" required>
                        <option <?php echo $acao === "Incluir" ? "selected" : ""?> value=0>Escolha a Prioridade da
                            Tarefa
                        </option>
                        <?php foreach ($prioridades as $prioridade): ?>
                        <option name="prioridade" value="<?=$prioridade['pri_id'] ?>"
                            <?php echo (isset($tarefa) && $prioridade['pri_id'] == $tarefa['tar_pri_id']) ? "selected" : ""?>>
                            <?=$prioridade['pri_nome'] ?>
                        </option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="categoria">Categoria:</label>
                    <select id="categoria" class="custom-select" name="categoria" required>
                        <option <?php echo $acao === "Incluir" ? "selected" : ""?> value=0>Escolha a Categoria da Tarefa
                        </option>
                        <?php
                        foreach($categorias as $categoria) :
                        ?>
                        <option name="status" value="<?=$categoria['cat_id']; ?>"
                            <?php echo (isset($tarefa) && $categoria['cat_id'] == $tarefa['tar_cat_id']) ? "selected" : ""?>>
                            <?=$categoria['cat_nome']; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="status">Status:</label>
                    <select id="status" class="custom-select" name="status" required>
                        <option <?php echo $acao === "Incluir" ? "selected" : ""?> value=0>Escolha o Status da Tarefa
                        </option>
                        <?php
                        foreach($status as $sta) :
                        ?>
                        <option name="status" value="<?=$sta['sta_id']; ?>"
                            <?php echo (isset($tarefa) && $sta['sta_id'] == $tarefa['tar_sta_id']) ? "selected" : ""?>>
                            <?=$sta['sta_nome']; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="d-inline-flex col-4">
                <label class="form-text">Data de Entrega:
                    <input class=" form-control" type="date" name="dataentrega" id="dataentrega" required
                        value="<?php echo (isset($tarefa) ? (string)$tarefa['tar_dataentrega'] : "") ?>">
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" name="concluida" value="1">
                <label class="form-check-label">Tarefa Concluída</label>
                <small class="form-text text-muted">Marcar quando concluída</small>
            </div>
        </fieldset>
        <div class="container-sm mt-3">
            <button class="btn btn-primary" type="submit" name="submit"> <i class="fas fa-save"></i> <?=$acao?></button>
            <a href="<?=base_url(($acao != "Incluir" ? "tarefa" : "/"))?>" class="btn btn-danger" type="submit"
                value="Limpar" name="reset"><i class="fas fa-broom"></i>
                <?=$acao != "Incluir" ? "Cancelar" : "Voltar" ?></a>
        </div>
    </form>
</div>