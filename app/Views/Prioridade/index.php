    <div class="container-sm mt-3">
        <!-- container1 -->
        <form method="post">
            <!-- form -->
            <fieldset class="form-group">
                <legend class="col-form-label">Nova Prioridade:</legend>
                <div class="form-group">
                    <label class="col-form-label" for="prioridade">Prioridade</label>
                    <input class="form-control" class="form-control is-invalid" type="text" name="prioridade"
                        id="prioridade" placeholder="Digite o nome da prioridade" required />
                </div>
                <div class="container-sm mt-3">
                    <button class="btn btn-primary" type="submit"><i class="fas fa-plus"></i><?=$acao?> </button>
                </div>
            </fieldset>
        </form>
        <div class="container-sm p-md-5">
            <!-- container2 -->
            <?php if (!is_null($prioridades)) : ?>
            <table class=" table table-hover table-striped text-center">
                <thead>
                    <th>Código</th>
                    <th>Prioridade</th>
                </thead>
                <tbody>
                    <?php foreach ($prioridades as $prioridade) : ?>
                    <tr>
                        <td><?php echo($prioridade['pri_id']); ?></td>
                        <td><?php echo($prioridade['pri_nome']); ?></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <?php endif; ?>
        </div>
    </div>