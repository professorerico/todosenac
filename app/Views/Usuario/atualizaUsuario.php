<!doctype html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.88.1">
    <title>Atualiza Usuário</title>

    <!-- Bootstrap core CSS -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet" />
    <!-- fontaewsome core css -->
    <link href="/assets/css/fontawesome.css" rel="stylesheet" />

    <style>
    .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    @media (min-width: 768px) {
        .bd-placeholder-img-lg {
            font-size: 3.5rem;
        }
    }
    </style>

    <link rel="stylesheet" href="/assets/css/style.css">

</head>

<body class="text-center">
    <form class="form-signin" method='post'>
        <h1 class="h3 mb-3 font-weight-normal">Atualização de Senha do Admin</h1>
        <input type="hidden" name="usu_id" value="<?=$usu_id?>">
        <label for="inputUsuario" class="sr-only">Usuário</label>
        <input type="text" id="inputUsuario" name="usuario" class="form-control" placeholder="Usuário"
            value="<?=$usu_cpf?>" readonly>
        <label for="inputPassword" class="sr-only">Senha</label>
        <input type="password" id="inputPassword" name="senha" class="form-control" placeholder="Password" required>
        <button type="submit" class=" btn btn-lg btn-primary btn-block"><i class="fas fa-sign-in-alt"></i>Atualiza
            Senha</button>
        <p class="mt-5 mb-3 text-muted">&copy; 2021</p>
    </form>



</body>

</html>