<?php

use Config\Validation;

$validacao = new Validation();
// $validacao->setRules(
//     [
//         'cpf' => 'required|min_length[8]',
//     ]
// );

helper('form');

echo form_open('',['method'=>'post',]);
    echo form_hidden(['usu_id'=>'']);
    echo form_label('CPF','cpf',['class'=>'form-label',]);
    echo form_input(['type'  => 'text',
    'name'  => 'cpf',
    'id'    => 'cpf',
    'value' => isset($usuario) ? $usuario->usu_cpf : '',
    'class' => 'form-control',]);
    echo form_label('Nome do usuário','nome',['class'=>'form-label']);
    echo form_input(['type'  => 'text',
        'name'  => 'nome',
        'id'    => 'nome',
        'value' => isset($usuario) ? $usuario->usu_nome : '',
        'class' => 'form-control',]);
    echo form_label('E-mail','e-mail',['class'=>'form-label',]);
    echo form_input(['type'  => 'email',
        'name'  => 'email',
        'id'    => 'e-mail',
        'value' => isset($usuario) ? $usuario->usu_email : '',
        'class' => 'form-control',]);
    echo form_label('Data de Nascimento','dt_nascimento',['class'=>'form-label',]);
    echo form_input(['type'  => 'date',
        'name'  => 'dt_nascimento',
        'id'    => 'dt_nascimento',
        'value' => isset($usuario) ? $usuario->usu_dt_nascimento : '',
        'class' => 'form-control',]);
    echo form_label('Senha','senha',['class'=>'form-label',]);
    echo form_input(['type'  => 'password',
        'name'  => 'senha',
        'id'    => 'senha',
        'class' => 'form-control',]);
    echo form_label('Confirme Senha','confsenha',['class'=>'form-label',]);
    echo form_input(['type'  => 'password',
        'name'  => 'confsenha',
        'id'    => 'confsenha',
        'class' => 'form-control',]);
echo form_close();