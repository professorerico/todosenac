    <div class="container-sm mt-3">
        <form method="post">
            <fieldset class="form-group">
                <legend class="col-form-label">Nova Categoria:</legend>
                <div class="form-group">
                    <label class="col-form-label" for="categoria">Categoria</label>
                    <input class="form-control" class="form-control is-invalid" type="text" name="categoria"
                        id="categoria" placeholder="Digite o nome da categoria" required />
                    <label class="col-form-label" for="descricao">Descrição</label>
                    <input class="form-control" class="form-control is-invalid" type="text" name="descricao"
                        id="descricao" placeholder="Digite a descrição da categoria" />
                </div>
                <div class="container-sm mt-3 form-group">
                    <button class="btn btn-primary" type="submit"><i class="fas fa-plus"></i><?=$acao?></button>
                </div>
            </fieldset>
        </form>
    </div>
    <?php if($msg != '') : ?>
    <div class="alert alert-info"><?=$msg?></div>
    <?php endif ?>
    <div class="container-sm p-md-5">
        <?php if (!is_null($categorias)) : ?>
        <table class="table table-hover table-striped text-center">
            <thead>
                <th>Código</th>
                <th>Status</th>
                <th>Descrição</th>
            </thead>
            <tbody>
                <?php foreach ($categorias as $categoria) : ?>
                <tr>
                    <td><?php echo($categoria['cat_id']); ?></td>
                    <td><?php echo($categoria['cat_nome']); ?></td>
                    <td><?php echo($categoria['cat_descricao']); ?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <?php endif; ?>
    </div>