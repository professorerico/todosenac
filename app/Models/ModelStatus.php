<?php

    namespace App\Models;

    use CodeIgniter\Model;

    class ModelStatus extends Model
    {
        protected $table = 'tb_status';
        protected $primaryKey = 'sta_id';

        protected $autoIncrement = true;

        protected $returnType = 'array';//object

        protected $allowedFields = ['sta_nome','sta_descricao'];

        protected $validationRules = [
            'sta_nome' => 'required|max_length[15]',
            'sta_descricao' => 'max_length[50]'
        ];

        protected $validationMessages = [
            'sta_nome' => [
                'required' => 'Nome Obrigatório',
                'max_length'=> 'Excedido o Limite [15]'
            ],
            'sta_descricao' => [
                'max_length' => 'Excedido o Limite [50]'
            ]
        ];
        protected $skipValidaton = false;

        public function get_status($sta_id)
        {
            $status = $this->find($sta_id);
            if(!is_null($status))
            {
                return $status['sta_nome'];
            }
            return "Não Definido";
        }

        public function lista_status($id = null)
        {
            $status = $this->find($id);
            return $status;
        }

        public function inserir(Array $novo)
        {
            if($novo['sta_descricao'] === '')
            {
                $novo['sta_descricao'] = 'Sem Descrição';
            }
            $this->set($novo);
            return $this->insert();
        }
    }