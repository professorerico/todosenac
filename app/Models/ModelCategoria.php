<?php
    namespace App\Models;

    use CodeIgniter\Model;

class ModelCategoria extends Model
{
    protected $table      = 'tb_categoria';
    protected $primaryKey = 'cat_id';

    protected $autoIncrement = true;

    protected $returnType = 'array';

    protected $allowedFields = ['cat_nome', 'cat_descricao'];

    public function get_categoria($cat_id)
        {
            $categoria = $this->find($cat_id);
            if(!is_null($categoria))
            {
                return $categoria['cat_nome'];
            }
            return "Não Definido";
        }

    public function lista_categoria($id = null)
    {
        return $this->find($id = null);
    }

    public function inserir(Array $nova)
    {
        if ($nova['cat_descricao'] === '')
        {
            $nova['cat_descricao'] = 'Sem Descrição';
        }

        $this->set($nova);
        return $this->insert();
    }
}