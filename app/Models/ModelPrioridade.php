<?php
    namespace App\Models;

    use CodeIgniter\Model;

class ModelPrioridade extends Model
{
    protected $table      = 'tb_prioridade';
    protected $primaryKey = 'pri_id';

    protected $autoIncrement = true;

    protected $returnType = 'array';

    protected $allowedFields = ['pri_nome'];

    public function get_prioridade($pri_id)
    {
        $Prioridade = $this->find($pri_id);
        if(!is_null($Prioridade))
            return $Prioridade['pri_nome'];
        return "Não Definido";
    }

    public function lista_Prioridade($id = null)
    {
        return $this->find($id = null);
    }

    public function inserir(Array $nova)
    {
        $this->set($nova);
        return $this->insert();
    }
}