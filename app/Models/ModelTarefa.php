<?php

   namespace App\Models;

   use CodeIgniter\Model;
   use App\Models\ModelCategoria as Categoria;
   use App\Models\ModelPrioridade as Prioridade;
   use App\Models\ModelStatus as Status;

   class ModelTarefa extends Model
   {
        protected $table      = 'tb_tarefa';
        protected $primaryKey = 'tar_id';
        protected $useAutoIncrement = true;

        protected $returnType = 'array';

        protected $allowedFields = [
            'tar_nome',
            'tar_descricao',
            'tar_pri_id',
            'tar_cat_id',
            'tar_sta_id',
            'tar_concluida',
            'tar_dataentrega',
        ];

        protected $validationRules    = [
            'tar_nome' => 'required|max_length[25]',
            'tar_descricao' => 'max_length[50]',
            'tar_pri_id' => 'required',
            'tar_cat_id' => 'required',
            'tar_sta_id' => 'required',
            'tar_dataentrega' => 'required',
        ];
        protected $validationMessages = [
            'tar_nome' =>[
                        'required' => '',
                        'max_length[25]' => '',
                    ],
            'tar_descricao' =>[
                        'max_length[50]' => '',
                    ],
            'tar_pri_id' =>[
                        'required'=> '',
                    ],
            'tar_cat_id' =>[
                        'required'=> '',
                    ],
            'tar_sta_id' =>[
                        'required'=> '',
                    ],
        ];

        protected $skipValidation     = false;

        public function get_concluida($id)
        {
            $tarefa = $this->find($id);
            if(!is_null($tarefa))
            {
                if($tarefa['tar_concluida'] == 1)
                    return 'Tarefa Concluída';
                else
                    return 'Tarefa Não Concluída';
            }
        }

        public function lista_tarefas($concluida = 0)
        {
            return $this->where('tar_concluida',$concluida)->findAll();
        }

        public function lista_tarefa($id)
        {
            return $this->find($id);
        }

        public function buscarCategoria($id = null)
        {
            $categoria = new Categoria();
            return $categoria->lista_categoria($id);
        }

        public function buscarPrioridade($id = null)
        {
            $prioridade = new Prioridade();
            return $prioridade->lista_prioridade($id);
        }

        public function buscarStatus($id = null)
        {
            $status = new Status();
            return $status->lista_status($id);
        }

        public function inserir(Array $tarefa)
        {
            $this->set($tarefa);
            return $this->insert();
        }

        public function atualizar($id, Array $tarefa)
        {
            return $this->update($id, $tarefa);
        }

        public function apagar($id)
        {
            return $this->delete($id);
        }
    }