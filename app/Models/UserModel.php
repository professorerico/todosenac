<?php

namespace App\Models;

use CodeIgniter\Model;

class UserModel extends Model
{
    protected $table                = 'tb_usuario';
    protected $primaryKey           = 'usu_id';
    protected $useAutoIncrement     = true;

    protected $returnType           = 'object';

    protected $useSoftDeletes       = false;

    protected $allowedFields        = [
        'usu_nome',
        'usu_cpf',
        'usu_senha',
        'usu_email',
        'usu_dt_nascimento',
    ];

    // Dates
    protected $useTimestamps        = true;
    protected $dateFormat           = 'datetime';
    protected $createdField         = 'created_at';
    protected $updatedField         = 'updated_at';
    protected $deletedField         = 'deleted_at';

    // Validation
    protected $validationRules      = [
        'usu_senha' => 'required',
    ];
    protected $validationMessages   = [
        'usu_senha' => [
            'required' => 'Senha Obrigatória',
        ]
    ];
    protected $skipValidation       = false;


    public function verificaAuth($auth)
    {
        $usuario = $this->where('usu_cpf',$auth['usuario'])->first();
        log_message('alert','Estou no verificaAuth do model');
        if(!is_null($usuario))
        {
            if($usuario->usu_nome === "admin" && is_null($usuario->updated_at))
            {
                return true;
            }

            $senhaHash = $usuario->usu_senha;

            if(password_verify($auth['senha'],$senhaHash))
            {
                return true;
            }

            return false;
        }
    }

    public function atualizaDados($dados)
    {
        log_message('critical','Estou no atualizaDados, '.$dados['usu_id']);
        $id = $dados['usu_id'];
        $dadosNovo = [
            'usu_cpf' => $dados['usu_cpf'],
            'usu_senha' => $dados['usu_senha'],
        ];

        return ($this->update($id,$dadosNovo));

    }

    public function listaUsuarios($id = null)
    {
        if(is_null($id))
            return $this->findAll();
        else
            return $this->find($id);

    }

}