<?php
    namespace App\Controllers;

    use App\Models\ModelPrioridade;

    class Prioridade extends BaseController
    {
        public function index()
        {
            $PrioridadeModel = new ModelPrioridade();
            $dadosPrioridade = [
                'titulo' => 'Prioridade',
                'msg' => '',
                'acao' => 'Inserir'
            ];

            if($this->request->getMethod() === 'post')
            {
                $novaPrioridade = [
                    'pri_nome' => $this->request->getPost('prioridade'),
                ];

                if($PrioridadeModel->inserir($novaPrioridade))
                {
                    $dadosPrioridade['msg'] = "{$novaPrioridade['pri_nome']} foi inserida";
                } else {
                    $dadosPrioridade['msg'] = "{$novaPrioridade['pri_nome']} não foi inserida";
                }
            }

            $dadosPrioridade['prioridades'] = $PrioridadeModel->lista_Prioridade();

            echo view('Base/topo');
            echo view('Prioridade/index',$dadosPrioridade);
            echo view('Base/base');
        }
    }