<?php

namespace App\Controllers;

class Principal extends BaseController
{
    public function index()
    {
        echo view('Base/topo');
        echo view('Principal/index');
        echo view('Base/base');
    }

    public function pesquisa($id)
    {
        $dados = [
            'identificador' => $id
        ];

        echo view('Base/topo');
        echo view('Principal/pesquisa', $dados);
        echo view('Base/base');
    }
}