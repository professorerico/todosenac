<?php

namespace App\Controllers;

use App\Controllers\BaseController;

use App\Models\UserModel;

class Login extends BaseController
{
    public function index()
    {
        $usuarioModel = new UserModel();

        if($this->request->getMethod() === 'post')
        {
            $auth = [
                "usuario" => $this->request->getPost('usuario'),
                "senha" => $this->request->getPost('senha'),
            ];
            $this->logger->alert('entrou no post de acesso');
            $usuario = $usuarioModel->where('usu_cpf',$auth['usuario'])->first();
            if($usuarioModel->verificaAuth($auth))
            {
                $this->logger->alert('entrou no verificaAuth true');
                $sessionLogin = [
                    'usuario' => $usuario->usu_nome,
                    'isLoggedIn' => true,
                ];
                session()->set($sessionLogin);

                if($usuario->usu_nome === 'admin' && is_null($usuario->updated_at))
                {
                    $this->logger->alert('entrou no verificaAuth true if do admin');
                    $usu_id = $usuario->usu_id;
                    return redirect()->to(base_url("UserController/atualizaUsuario/{$usu_id}"));
                }
                return redirect()->to(base_url());
            }
            session()->setFlashdata('msg', 'Usuário ou senha inválidos!');
        }

        return view('Login/login');
    }

    public function sair()
    {
        $sessionLogin = ['usuario','isLoggedIn' ];
        session()->remove($sessionLogin);
        return redirect()->to(base_url());
    }
}