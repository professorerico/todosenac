<?php
    namespace App\Controllers;

    use App\Models\ModelCategoria;

    class Categoria extends BaseController
    {
        public function index()
        {
            $categoriaModel = new ModelCategoria();
            $dadosCategoria = [
                'titulo' => 'Categoria',
                'msg' => '',
                'acao' => 'Inserir'
            ];

            if($this->request->getMethod() === 'post')
            {
                $novaCategoria = [
                    'cat_nome' => $this->request->getPost('categoria'),
                    'cat_descricao' => $this->request->getPost('descricao')
                ];

                if($categoriaModel->inserir($novaCategoria))
                {
                    $dadosCategoria['msg'] = "{$novaCategoria['cat_nome']} foi inserida";
                } else {
                    $dadosCategoria['msg'] = "{$novaCategoria['cat_nome']} não foi inserida";
                }
            }

            $dadosCategoria['categorias'] = $categoriaModel->lista_categoria();

            echo view('Base/topo');
            echo view('Categoria/index',$dadosCategoria);
            echo view('Base/base');
        }
    }