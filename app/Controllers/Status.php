<?php
    namespace App\Controllers;

    use App\Models\ModelStatus;

    class Status extends BaseController
    {
        public function index()
        {
            $statusModel = new ModelStatus();

            $dadosStatus = [
                'titulo' => 'Status',
                'msg' => '',
                'status' => [],
                'acao' => 'Inserir',
                'erros'=>'',
            ];

            if($this->request->getMethod() === 'post')
            {
                $novoStatus = [
                    'sta_nome' => $this->request->getPost('status'),
                    'sta_descricao' => $this->request->getPost('descricao'),
                ];

                if($statusModel->inserir($novoStatus))
                {
                    $dadosStatus['msg'] = "{$novoStatus['sta_nome']} foi inserido";
                } else {
                    $dadosStatus['msg'] = $statusModel->errors();
                    $dadosStatus['novoStatus'] = $novoStatus;
                }

            }

            $dadosStatus['status'] = $statusModel->lista_status();

            echo view('Base/topo');
            echo view('Status/lista', $dadosStatus);
            echo view('Base/base');
        }
    }