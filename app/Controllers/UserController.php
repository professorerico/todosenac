<?php
namespace App\Controllers;

use App\Models\UserModel;

class UserController extends BaseController
{
    public function index()
    {
        $usuarios = New UserModel();

        $dadosUsuarioFormulario = [
            'acao' => '',
            'cpf' => '',
            'nome' => '',
            'email' => '',
            'dt_nascimento' => '',
            'senha' => '',
        ];

        $dadosUsuarios = [
            'acao' => '',
            'usuarios' => $usuarios->listaUsuarios(),
        ];

        if($this->request->getMethod() === 'post')
        {
            $dadosUsuario = [
                'usu_id' => $this->request->getPost('id'),
                'usu_cpf' => $this->request->getPost('cpf'),
                'usu_nome' => $this->request->getPost('nome'),
                'usu_email' => $this->request->getPost('email'),
                'usu_dt_ascimento' => $this->request->getPost('dt_nascimento'),
                'usu_senha' => $this->request->getPost('senha'),
            ];
        }

        echo view('Base/topo');
        echo view('Usuario/usuario',$dadosUsuarioFormulario);
        echo view('Usuario/usuarios',$dadosUsuarios);
        echo view('Base/base');
    }

    public function atualizaUsuario(int $id)
    {
        $this->logger->info('Estou no atualizaUsuario');
        $usuarioModel = new UserModel();
        $usuario = $usuarioModel->find($id);

        $dados = [
            'usu_id' => $usuario->usu_id,
            'usu_cpf' => $usuario->usu_cpf ,
            'usu_senha' => $usuario->usu_senha,
            'erros' => null,
        ];

        if($this->request->getMethod() === 'post')
        {
            $dados = [
                'usu_id' => $this->request->getPost('usu_id'),
                'usu_cpf' => $this->request->getPost('usuario'),
                'usu_senha' => password_hash($this->request->getPost('senha'),PASSWORD_DEFAULT),
            ];
            log_message('info','Estou no post atualizaUsuario');
            if($usuarioModel->atualizaDados($dados))
            {
                return redirect()->to(base_url());
            }
            else
            {
                $dados['erros'] = $usuarioModel->errors();
            }
        }
        return view('Usuario/atualizaUsuario',$dados);
    }

    public function atualizaSenha($id)
    {
        $usuarioModel = new UserModel();

    }


}