<?php
namespace App\Controllers;

use App\Models\ModelTarefa;

class Tarefa extends BaseController
{
    public function index()
    {
        $tarefaModel = new ModelTarefa();

        $dadosTarefaFormulario = [
            'tarefa' => [
                'tar_id' => '',
                'tar_nome' => '',
                'tar_descricao' => '',
                'tar_pri_id' => '',
                'tar_cat_id' => '',
                'tar_sta_id' => '',
                'tar_concluida' => '',
                'tar_dataentrega' => '',
            ],
            'prioridades' => $tarefaModel->buscarPrioridade(),
            'categorias' => $tarefaModel->buscarCategoria(),
            'status'=> $tarefaModel->buscarStatus(),
            'acao' => 'Incluir',
        ];
        $dadosTarefas = [
            'msg' => '',
            'opcao' => 'Ações',
            'tarefas' => $tarefaModel->lista_tarefas(),
        ];

        if($this->request->getMethod() === 'post')
        {
            $tarefa = [
                'tar_nome' => $this->request->getPost('tarefa'),
                'tar_descricao' => $this->request->getPost('descricao'),
                'tar_pri_id' => $this->request->getPost('prioridade'),
                'tar_cat_id' => $this->request->getPost('categoria'),
                'tar_sta_id' => $this->request->getPost('status'),
                'tar_concluida' => $this->request->getPost('concluida') != '1' ? 0 : 1,
                'tar_dataentrega' => $this->request->getPost('dataentrega'),
            ];
            if ($tarefaModel->inserir($tarefa))
            {
                $dadosTarefaFormulario['msg'] = "Tarefa inserida";
                $this->logger->info('{tar_nome} Inserida', $tarefa);
            }else
            {
                $dadosTarefaFormulario['msg'] = 'Ocorreu um Erro! Tarefa não inserida';
                $erros = [
                    'tarefa' => $tarefa['tar_nome'],
                    'erro' => error_get_last(),
                ];
                $this->logger->error('{tarefa} Não Inserida, erro: {erro}', $erros);
            }
            $dadosTarefas['tarefas'] = $tarefaModel->lista_tarefas();
        }

        echo view('Base/topo');
        echo view('Tarefa/tarefa',$dadosTarefaFormulario);
        echo view('Tarefa/tarefas',$dadosTarefas);
        echo view('Base/base');
    }


    public function concluidas()
    {
        $tarefaModel = new ModelTarefa();


        $dadosTarefas = [
            'msg' => 'Tarefas Concluídas',
            'opcao' => 'Concluída',
            'tarefas' =>  $tarefaModel->lista_tarefas(1),
        ];

        echo view('Base/topo');
        echo view('Tarefa/tarefas',$dadosTarefas);
        echo view('Base/base');
    }

    public function atualizarTarefa($id)
    {
        $tarefaModel = new ModelTarefa();

        $dadosTarefaFormulario = [
            'msg' => 'Atualizar Tarefa',
            'acao' => 'Atualizar',
            'tarefa' => $tarefaModel->lista_tarefa($id),
            'prioridades' => $tarefaModel->buscarPrioridade(),
            'categorias' => $tarefaModel->buscarCategoria(),
            'status'=> $tarefaModel->buscarStatus(),
        ];

        if(!is_null($id))
        {
            $dados['original'] = $tarefaModel->lista_tarefa($id)['tar_nome'];

            if($this->request->getMethod() === 'post')
            {
                $tarefa = [
                    'tar_nome' => $this->request->getPost('tarefa'),
                    'tar_descricao' => $this->request->getPost('descricao'),
                    'tar_pri_id' => $this->request->getPost('prioridade'),
                    'tar_cat_id' => $this->request->getPost('categoria'),
                    'tar_sta_id' => $this->request->getPost('status'),
                    'tar_concluida' => $this->request->getPost('concluida') != '1' ? 0 : 1,
                    'tar_dataentrega' => $this->request->getPost('dataentrega'),
                ];
                $dados['novo'] = $tarefa['tar_nome'];
                $dadosTarefaFormulario['tarefa'] = $tarefa;
                $tarefaModel->atualizar($id, $tarefa);
                $this->logger->alert('Tarefa: Atualizada de {original} para {novo}',$dados);
                return redirect()->to(base_url("tarefa"));
            }
        }
        echo view('Base/topo');
        echo view('Tarefa/tarefa',$dadosTarefaFormulario);
        echo view('Base/base');
    }

    public function apagarTarefa($id)
    {

        $tarefaModel = new ModelTarefa();
        $dados['tarefa'] = $tarefaModel->lista_tarefa($id)['tar_nome'];
        if($tarefaModel->apagar($id))
        {
            $dados['msg'] = 'Tarefa Apagada';
        } else
        {
            $dados['msg'] = 'Ocorreu um erro! Tarefa não Apagada';
        }
        $this->logger->alert('Exclusão de: {tarefa}: Info {msg}',$dados);
        return redirect()->to(base_url("tarefa"));
    }
}